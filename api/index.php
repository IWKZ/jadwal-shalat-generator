<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization');

function csvToArray($filename='', $delimiter=' ')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 0, $delimiter)) !== FALSE)
        {
            if(!$header)
            {
               $header = $row;
            }
            else
            {
                if(count($header)!=count($row)){ continue; }

                $data[] = array_combine($header, $row);
            }
        }
        fclose($handle);
    }
    return $data;
}

$jadwalShalat = csvToArray('data.csv');

if (isset($_GET['monthly'])) {
    $currentMonth = '.'.date('m');

    if(isset($_GET['month'])) {
        $currentMonth = '.'.$_GET['month'];
    }

    $data = [];

    foreach($jadwalShalat as $jadwal) {
        if(strpos($jadwal['date'], $currentMonth) !== false) {
            $data[$jadwal['date']] = $jadwal;
        }
    }

    echo json_encode($data);
    return;
} else {
    $todayDateKey = date('d').'.'.date('m');

    foreach($jadwalShalat as $jadwal){
        if($jadwal['date'] == $todayDateKey){
            echo json_encode($jadwal);
            break;
        }
    }
}
?>
