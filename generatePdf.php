<?php
include "env.php";

$months = ["01" => "Januari","02" => "Februari","03" => "Maret","04" => "April","05" => "Mei","06" => "Juni","07" => "Juli","08" => "Agustus","09" => "September","10" => "Oktober","11" => "November","12" => "Desember"];

if(isset($_POST['generate'])) {
  $params = "";
  $fileName = "Jadwal-Shalat";

  if (isset($_POST['month']) && isset($_POST['year'])) {
    $params = "?month={$_POST['month']}&year={$_POST['year']}";
    $fileName .= "-{$months[$_POST['month']]}-{$_POST['year']}"; 
  }

  $data = [
    'url' => "{$ROOT_URL}{$params}",
    'apiKey' => $PDF_GENERATOR_KEY
  ];
  $dataString = json_encode($data);

  $ch = curl_init($API_PDF_GENERATOR_URL);                                                                      
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');                                                                     
  curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Content-Length: ' . strlen($dataString),
  ]);

  $response = curl_exec($ch);
  $err = curl_error($ch);

  curl_close($curl);

  if ($err) {
    echo 'Error #:' . $err;
  } else {
    header("Content-Disposition: attachment; filename={$fileName}.pdf");
    echo $response;
  }
}
?>

<!doctype html>
<html>
<head>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <form action="" method="post">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="bulan">Bulan</label>
          <select class="form-control" id="bulan" name="month">
            <?php foreach($months as $key=>$month) {?>
              <option value="<?= $key ?>" <?php echo "".$key === date('m') ? 'selected' : ''; ?>>
                <?= $month ?>
              </option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="tahun">Tahun</label>
          <input type="text" name="year" id="tahun" class="form-control" value="<?= date('Y') ?>">
        </div>
      </div>
      <input type="hidden" name="generate" value="true" />
      <button type="submit" class="btn btn-primary">Generate</button>
    </form>
  </div>
</body>
</html>