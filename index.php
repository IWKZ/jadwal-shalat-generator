<?php
include "env.php";

$api = "{$API_URL}/?monthly=true";
$apiHijriah = $API_HIJRIAH_DATE_URL;
$month = date('m');
$year = date('Y');
$hijriahMonths = ["Muharram" => "Muharram","Safar" => "Shafar","Rabi ul Awal" => "Rabiul Awal","Rabi Al-Akhar" => "Rabiul Akhir","Jumada Al-Awwal" => "Jumadil Awal","Jumada Al-Akhirah" => "Jumadil Akhir","Rajab" => "Rajab","Shaban" => "Sya'ban","Ramadan" => "Ramadhan","Shawwal" => "Syawwal","Dhul Qadah" => "Dzulkaidah","Dhul Hijjah" => "Dzulhijjah"];
$months = ["01" => "Januari","02" => "Februari","03" => "Maret","04" => "April","05" => "Mei","06" => "Juni","07" => "Juli","08" => "Agustus","09" => "September","10" => "Oktober","11" => "November","12" => "Desember"];

if (isset($_GET['month']) && (int)$_GET['month'] <= 12 && (int)$_GET['month'] > 0) {
  $month = $_GET['month'];
  $month = sprintf("%02d", $month);
  $api .= "&month={$month}";
}
if (isset($_GET['year'])) {
  $year = $_GET['year'];
}

$jadwalSholat = json_decode(file_get_contents($api), true);
$hijriahData = getHijriahData();

$data = [];
$index = 1;
foreach($jadwalSholat as $jadwal) {
  $data[] = [
    'date' => "{$jadwal['date']}.{$year}",
    'hijriahDate' => getHijriahDate($index),
    'subuh' => $jadwal['subuh'],
    'terbit' => $jadwal['terbit'],
    'dzuhur' => $jadwal['dzuhur'],
    'ashr' => $jadwal['ashr'],
    'maghrib' => $jadwal['maghrib'],
    'isya' => $jadwal['isya'],
  ];

  $index++;
}

$monthLabel = $months[$month];
$hijriahMonthLabel = getHijriahMonthLabel();
$hijriahYearLabel = getHijriahYearLabel();

function getHijriahMonthLabel() {
  global $hijriahMonths;
  global $hijriahData;

  if (empty($hijriahData)) return "";

  return "{$hijriahMonths[$hijriahData['firstMonth']]} - {$hijriahMonths[$hijriahData['secondMonth']]}";
}

function getHijriahYearLabel() {
  global $hijriahData;

  if (empty($hijriahData)) return "";

  $hijriahYear = $hijriahData['firstMonthYear'] === $hijriahData['secondMonthYear']
    ? $hijriahData['firstMonthYear']
    : "{$hijriahData['firstMonthYear']} / {$hijriahData['secondMonthYear']}";

  return "{$hijriahYear} H";
}

function getHijriahDate($day) {
  global $hijriahData;
  $startDate = 1;

  if (empty($hijriahData)) return 0;

  $firstHijriahDate = $hijriahData['secondMonthStartDate'];
  $secondChangeHijriahDateOn = $hijriahData['secondaryMonthChangedate'];
  $firstChangeHijriahDateOn = $hijriahData['firstMonthChangedate'];

  $changeHijriahDateOn = !empty($firstChangeHijriahDateOn) && $day >= $firstChangeHijriahDateOn && $day < $secondChangeHijriahDateOn
    ? $firstChangeHijriahDateOn : $secondChangeHijriahDateOn;
  
  $hijriahDate = ($day - 1) + $firstHijriahDate;
  
  if ($day >= $changeHijriahDateOn) {
    $hijriahDate = ($day - $changeHijriahDateOn) + 1;
  }

  return sprintf("%02d", $hijriahDate);
}

function getHijriahData() {
  global $month;
  global $year;
  global $API_HIJRIAH_DATE_URL;

  try {
    $tmpDbFile = 'db.json';
    $monthName = date('F', mktime(0, 0, 0, $month, 10));
    $tmpDataKey = $month.$year;
    $tmpData = json_decode(file_get_contents($tmpDbFile), true);

    if (!isset($tmpData[$tmpDataKey])) {
      $apiUrl = $API_HIJRIAH_DATE_URL."&month={$monthName}&year={$year}";
      $hijriahData = json_decode(file_get_contents($apiUrl), true);
    
      $tmpData[$tmpDataKey] = $hijriahData;

      // only record data if year between 2019 - 2040
      if ($year >= 2019 && $year <= 2040) {
        file_put_contents($tmpDbFile, json_encode($tmpData, JSON_PRETTY_PRINT));
      }
    }

    return $tmpData[$tmpDataKey];
  } catch(Exception $e) {
    return null;
  }
};
?>


<!DOCTYPE html>
<html lang="en">
<head>
<style>
.responstable {
  margin: 1em 0;
  width: 100%;
  overflow: hidden;
  background: #fff;
  color: #006090;
  border-radius: 10px;
  border: 1px solid #167f92;
}
.responstable tr {
  border: 1px solid #d9e4e6;
}
.responstable tr:nth-child(odd) {
  background-color: #eaf3f3;
}
.responstable th {
  display: none;
  border: 1px solid #fff;
  background-color: #167f92;
  color: #fff;
  padding: 1em;
}
.responstable th:first-child {
  display: table-cell;
  text-align: center;
}
.responstable th:nth-child(2) {
  display: table-cell;
}
.responstable th:nth-child(2) span {
  display: none;
}
.responstable th:nth-child(2):after {
  content: attr(data-th);
}
@media (min-width: 480px) {
  .responstable th:nth-child(2) span {
    display: block;
  }
  .responstable th:nth-child(2):after {
    display: none;
  }
}
.responstable td {
  display: block;
  word-wrap: break-word;
  max-width: 7em;
}
.responstable td:first-child {
  display: table-cell;
  text-align: center;
  border-right: 1px solid #d9e4e6;
}
@media (min-width: 480px) {
  .responstable td {
    border: 1px solid #d9e4e6;
  }
}
.responstable th,
.responstable td {
  text-align: center;
  margin: 0.5em 1em;
}
@media (min-width: 480px) {
  .responstable th,
  .responstable td {
    display: table-cell;
    padding: 0.1em;
  }
}

body {
  padding: 0 2em;
  font-family: Arial, sans-serif;
  color: #3b444b;
}

h1 {
  font-weight: normal;
  color: #024457;
  text-align: center;
}
h1 span {
  color: #167f92;
}

.header {
  height: 100px;
  display: flex;
  flex-direction: row;
  justify-content: center;
}

.headerImage {
  width: 15%;
  margin-right: 50px;
}

.headerContent {
  margin-top: 10px;
}

.headerContent h2 {
  line-height: 10px;
  color: #024457;
  text-align: center;
  font-size: 1.3em;
}

.monthInfo {
  margin-top: -20px;
}

footer {
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  color: #306078;
}

.leftFooter p{
  line-height: 0.3;
  margin-top: 7px;
}

.rightFooter {
  flex: 0 0 350px;
  text-align: center;
}

.rightFooter p {
  line-height: 0.5;
}

.footerImage {
  width: 20%;
}

</style>


</head>

<body>
  <div class="header">
    <img class="headerImage" src="http://cdn-radiotime-logos.tunein.com/s271525g.png">
    <div class="headerContent">
      <h2>Masjid Al-Falah Berlin - IWKZ e.V.</h2>
      <h2><?= $monthLabel ?> <?= $year ?> <?php echo empty($hijriahMonthLabel) ? '' : '/ '.$hijriahMonthLabel ?> <?= $hijriahYearLabel ?></h2>
    </div>
  </div>
  <h1 class="monthInfo">
  </h1>
  <hr>
  <table id="jadwalShalat" class="responstable">
    <tr>
      <th><?= $monthLabel ?></th>
      <?php echo empty($hijriahMonthLabel) ? '' : "<th>{$hijriahMonthLabel}</th>" ?>
      <th>Subuh</th>
      <th>Terbit</th>
      <th>Dzuhur</th>
      <th>Ashr</th>
      <th>Maghrib</th>
      <th>Isya</th>
    </tr>
    <tbody>
        <?php
          foreach($data as $d) { ?>
          <tr>
          <td><?= $d['date'] ?></td>
          <?php echo empty($d['hijriahDate']) ? '' : "<td>{$d['hijriahDate']}</td>" ?>
          <td><?= $d['subuh'] ?></td>
          <td><?= $d['terbit'] ?></td>
          <td><?= $d['dzuhur'] ?></td>
          <td><?= $d['ashr'] ?></td>
          <td><?= $d['maghrib'] ?></td>
          <td><?= $d['isya'] ?></td>
        </tr>
        <?php } ?>
    </tbody>
  </table>

  <hr>

  <footer>
    <div class="leftFooter">
      <p>Feldzeugmeister 1</p>
      <p>10557 Berlin</p>
      <p>Tlp: (030) 6792 7147</p>
      <p>www.iwkz.de - info@iwkz.de</p>
      <p>prs.iwkz.de</p>
    </div>
    <div class="rightFooter">
      <img class="footerImage" src="https://iwkz.de/jadwalsholat/wa-img.png">
      <p>WA Broadcast Masjid Al-Falah</p>
      <p><b>+4915223122739</b> - WA: <b>Nama Anda</b> (spasi) <b>Ya</b></p>
    </div>
  </footer>

</body>
</html>
